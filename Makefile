# Un perfil genérico que recibe un número de 2 dígitos y compila y ejecuta el
# archivo tests/test_XX.c y exXX/*.c siendo XX el número de 2 dígitos recibido.
# Ejemplo:	make 01
# Ejecuta: 	cc tests/test_01.c ex01/*.c -Wall -Werror -Wextra -o tests/test_01
#          	./tests/test_01
#			rm tests/test_01
%:
	@clear
	@echo "Ejecutando TEST $@:\t$$(basename ex$@/*.c '.c')\n"
	@cc tests/test_$@.c ex$@/*.h -Wall -Werror -Wextra -o tests/test_$@
	@./tests/test_$@ AH
	@rm tests/test_$@
